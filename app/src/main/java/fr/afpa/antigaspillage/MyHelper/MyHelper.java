package fr.afpa.antigaspillage.MyHelper;

/**
 * Created by Elvis on <DATE-DU-JOUR>.
 */
import java.util.ArrayList;

import fr.afpa.antigaspillage.model.Product;
import io.realm.Realm;
import io.realm.RealmResults;

public class MyHelper {

    Realm realm;
    RealmResults<Product> product;

    public MyHelper(Realm realm) {
        this.realm = realm;
    }

    public void selectNameFromDB(){
        product = realm.where(Product.class).findAll();
    }
    public ArrayList<Product> justRefresh(){

        ArrayList<Product> listItem = new ArrayList<>();
        for (Product p : product){
            listItem.add(p);
        }

        return listItem;
    }

}