package fr.afpa.antigaspillage;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;

import java.util.Timer;
import java.util.TimerTask;

public class MainActivity<timeout> extends AppCompatActivity {
    public static final String NOTIFICATION_CHANNEL_ID = "10001" ;//a
    private final static String default_notification_channel_id = "default" ;//a
    int timeout = 3000; // delai de 3secondes
    Timer timer = new Timer();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        timer.schedule(new  TimerTask() {

            @Override
            public void run() {
                finish();
                Intent intent = new Intent(MainActivity.this, ListPicture.class);
                startActivity(intent);
            }
        }, timeout);
    }

}