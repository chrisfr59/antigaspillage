package fr.afpa.antigaspillage.model;


import android.widget.DatePicker;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Elvis on <DATE-DU-JOUR>.
 */


public class Product extends RealmObject {

    @PrimaryKey
    private int product_id;
    private String product_name;
    private Date date;
    private String image;
    private String numberRemaining;

    public int getProduct_id() {
        return product_id;
    }
    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public String getProduct_name() {
        return product_name;
    }
    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public Date getDate(){return date;}
    public void setDate(Date date) {this.date = date;}

    public String getImage() {
        return image;
    }
    public void setImage(String image) {
        this.image = image;
    }

    public String getNumberRemaining(){return numberRemaining;}
    public void setNumberRemaining(String numberRemaining){this.numberRemaining = numberRemaining;}
}

