package fr.afpa.antigaspillage;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import fr.afpa.antigaspillage.model.Product;
import io.realm.Realm;

public class DetailActivity extends AppCompatActivity {
    private static final SimpleDateFormat DATE_TIME_FORMATTER = new SimpleDateFormat("dd/MM/yyyy");
    Realm realm;
    EditText editNameDetail;
    EditText editDateDetail;
    Button btnUpdate, btnDelete;
    Product product;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        realm = Realm.getDefaultInstance();
        editNameDetail = findViewById(R.id.edit_nameDetail);
        editDateDetail = findViewById(R.id.edit_dateDetail);
        btnUpdate = findViewById(R.id.btn_update);
        btnDelete = findViewById(R.id.btn_delete);

        Intent getintent = getIntent();
        int position = getintent.getIntExtra("numPosition", 0);

        product = realm.where(Product.class).equalTo("product_id", position).findFirst();

        editNameDetail.setText(product.getProduct_name());
        editDateDetail.setText(product.getDate() == null ? "Pas de date" : DATE_TIME_FORMATTER.format(product.getDate()));
        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateData();
                onBackPressed();
            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteData();
                onBackPressed();
            }
        });
    }

    private void deleteData(){

        realm.beginTransaction();
        product.deleteFromRealm();
        realm.commitTransaction();
    }

    private void updateData(){

        realm.beginTransaction();
        product.setProduct_name(editNameDetail.getText().toString());
        try {
            product.setDate(DATE_TIME_FORMATTER.parse(editDateDetail.getText().toString()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        realm.commitTransaction();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void onClickDateField(View view) {
        final Calendar cldr = Calendar.getInstance();
        int day = cldr.get(Calendar.DAY_OF_MONTH);
        int month = cldr.get(Calendar.MONTH);
        int year = cldr.get(Calendar.YEAR);
        // date picker dialog
        new DatePickerDialog(this, (pickerView, pickerYear, monthOfYear, dayOfMonth) ->
                editDateDetail.setText(DATE_TIME_FORMATTER.format(new Date(pickerYear - 1900, monthOfYear, dayOfMonth)))
                , year, month, day)
                .show();
    }
}