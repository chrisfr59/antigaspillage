package fr.afpa.antigaspillage;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import fr.afpa.antigaspillage.MyHelper.MyHelper;
import fr.afpa.antigaspillage.customadapter.CustomAdapter;
import io.realm.Realm;
import io.realm.RealmChangeListener;

public class ListPicture extends AppCompatActivity {
    private static final int REQUEST_IMAGE_CAPTURE_ID = 1234;
    Realm realm;
    ListView listView;
    MyHelper helper;
    RealmChangeListener realmChangeListener;
    private ImageView imageFeedbackImageView;
    // recupere cette image pour envoyer dans une autre activity
    private String imagePath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_picture);

        listView = findViewById(R.id.listView);

        realm = Realm.getDefaultInstance();
        helper = new MyHelper(realm);
        helper.selectNameFromDB();

        CustomAdapter adapter = new CustomAdapter(this, helper.justRefresh());
        listView.setAdapter(adapter);

        Refresh();
    }


    private void Refresh() {
        realmChangeListener = new RealmChangeListener() {
            @Override
            public void onChange(Object o) {
                CustomAdapter adapter = new CustomAdapter(ListPicture.this, helper.justRefresh());
                listView.setAdapter(adapter);
            }
        };
        realm.addChangeListener(realmChangeListener);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.removeChangeListener(realmChangeListener);
        realm.close();
    }

    public void onClickAddProduct(View view) {
        onClickOpenCamera(view);
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        @SuppressLint("SimpleDateFormat") String timeStamp = new SimpleDateFormat("yyyyMMdd").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        return File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
    }

    public void onClickOpenCamera(View view) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                imagePath = photoFile.getAbsolutePath();
                Uri photoURI = FileProvider.getUriForFile(this,
                        "fr.afpa.antigaspillage.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE_ID);
            }

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE_CAPTURE_ID && resultCode == RESULT_OK) {
            Intent intent = new Intent(this, Detailsproduct.class);
            intent.putExtra("bitmap", imagePath);
            startActivity(intent);
        }
    }

    public void onClickButtonSave(View view) {
        Intent intent = new Intent(this, Detailsproduct.class);
        intent.putExtra("bitmap", imagePath);
        startActivity(intent);
    }
}