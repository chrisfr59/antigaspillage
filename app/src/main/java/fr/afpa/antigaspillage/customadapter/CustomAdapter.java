package fr.afpa.antigaspillage.customadapter;

/**
 * Created by Elvis on <DATE-DU-JOUR>.
 */

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;


import fr.afpa.antigaspillage.DetailActivity;
import fr.afpa.antigaspillage.R;
import fr.afpa.antigaspillage.model.Product;


public class CustomAdapter extends BaseAdapter {
    private static final SimpleDateFormat DATE_TIME_FORMATTER = new SimpleDateFormat("dd/MM/yyyy");
    final Date now = Calendar.getInstance().getTime();

    // recupere cette image pour envoyer dans une autre activity

    Context c;
    ArrayList<Product> product;

    public CustomAdapter(Context c, ArrayList<Product> product) {
        this.c = c;
        this.product = product;
    }

    @Override
    public int getCount() {
        return product.size();
    }

    @Override
    public Object getItem(int position) {
        return product.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.list_product_adapter, parent, false);

        final TextView productNameTextView = view.findViewById(R.id.productName);
        final TextView peremptionTextView = view.findViewById(R.id.peremption);
        final ImageView productImageView = view.findViewById(R.id.productImageView);
        final TextView numberRemaining = view.findViewById(R.id.numberRemaining);

        Product p = (Product) this.getItem(position);
        final int numPosition = p.getProduct_id();
        productNameTextView.setText(p.getProduct_name());

       numberRemaining.setText(p.getNumberRemaining());

        String result = p.getDate() == null ? "" : String.valueOf(daysBetween(now, p.getDate()));


        int inum = Integer.parseInt(result);
        //
        if (inum <= 0 ){

            numberRemaining.setTextColor(Color.RED);

        }else{
            numberRemaining.setTextColor(Color.GREEN);

        }


        numberRemaining.setText(result  + " days remaining");
        peremptionTextView.setText(p.getDate() == null ? "Pas de date" : DATE_TIME_FORMATTER.format(p.getDate()));




        view.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                if (p.getImage() != null) {
                    setPic(p.getImage(), productImageView);
                }
            }
        });

        //OnClick
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(c, DetailActivity.class);
                intent.putExtra("numPosition", numPosition);
                c.startActivity(intent);
            }
        });

        return view;

    }


    private void setPic(String imagePath, ImageView imageFeedbackImageView) {
        // Get the dimensions of the View
        int targetW = imageFeedbackImageView.getWidth();
        int targetH = imageFeedbackImageView.getHeight();

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;

        BitmapFactory.decodeFile(imagePath, bmOptions);

        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.max(1, Math.min(photoW / targetW, photoH / targetH));

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(imagePath, bmOptions);
        imageFeedbackImageView.setImageBitmap(Bitmap.createScaledBitmap(bitmap, 100, 100, false));

    }

    private int daysBetween(Date d1, Date d2) {
        int numberRemaining = (int) ((d2.getTime() - d1.getTime()) / (1000 * 60 * 60 * 24) + 1);
        return numberRemaining;
    }


}
