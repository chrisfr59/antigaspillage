package fr.afpa.antigaspillage;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;

import fr.afpa.antigaspillage.MyHelper.MyHelper;
import fr.afpa.antigaspillage.model.Product;
import io.realm.Realm;
import io.realm.RealmChangeListener;

public class Detailsproduct extends AppCompatActivity {

    private static final SimpleDateFormat DATE_TIME_FORMATTER = new SimpleDateFormat("dd/MM/yyyy");

    private String imagePath;
    private ImageView imageView;
    Realm realm;
    Button btnSave;
    EditText nameProduct;
    EditText datePeremption;
    MyHelper helper;
    RealmChangeListener realmChangeListener;
    TextView numberRemaining;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailsproduct);

        realm = Realm.getDefaultInstance();
        nameProduct = findViewById(R.id.nameProduct);
        datePeremption = findViewById(R.id.datePeremption);
        btnSave = findViewById(R.id.btnSave);
        imageView = findViewById(R.id.productImageView);
        numberRemaining = findViewById(R.id.numberRemaining);
        helper = new MyHelper(realm);
        helper.selectNameFromDB();

        Refresh();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void saveData(View view) {
        datePeremption.setError("");

        try {

            final Date now = Calendar.getInstance().getTime();
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.YEAR, 10);
            final Date future = calendar.getTime();
            final Date date = DATE_TIME_FORMATTER.parse(datePeremption.getText().toString());

            if (now.after(date)) {
                datePeremption.setError("The date should be in the future");
                return;
            } else if (future.before(date)) {
                datePeremption.setError("The date should be before than ten years later");
                return;
            }

            realm.executeTransactionAsync(new Realm.Transaction() {
                @Override
                public void execute(Realm bgRealm) {
                    Number maxId = bgRealm.where(Product.class).max("product_id");

                    int newKey = (maxId == null) ? 1 : maxId.intValue() + 1;

                    Product product = bgRealm.createObject(Product.class, newKey);
                    product.setProduct_name(nameProduct.getText().toString());
                    product.setImage(getIntent().getStringExtra("bitmap"));
                    product.setDate(date);

                    scheduleNotification(product);
                }


                //}
            }, new Realm.Transaction.OnSuccess() {
                @Override
                public void onSuccess() {
                    // Transaction was a success.
                    Toast.makeText(Detailsproduct.this, "Bravo", Toast.LENGTH_LONG).show();
                }
            }, new Realm.Transaction.OnError() {
                @Override
                public void onError(Throwable error) {
                    // Transaction failed and was automatically canceled.
                    Toast.makeText(Detailsproduct.this, "Nope..", Toast.LENGTH_LONG).show();
                }
            });
        } catch (ParseException e) {
            datePeremption.setError("The date should be defined");
            e.printStackTrace();


        }
    }




    private void Refresh() {
        realmChangeListener = o -> {
            // Change de page
            Intent intent = new Intent(Detailsproduct.this, ListPicture.class);
            startActivity(intent);
        };
        realm.addChangeListener(realmChangeListener);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.removeChangeListener(realmChangeListener);
        realm.close();
    }


    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        if (hasFocus) {
            String bitmap = getIntent().getStringExtra("bitmap");
            setPic(bitmap);
        }
    }

    // pour sauvegarder la photo
    private void setPic(String imagePath) {
        // Get the dimensions of the View
        int targetW = imageView.getWidth();
        int targetH = imageView.getHeight();

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;

        BitmapFactory.decodeFile(imagePath, bmOptions);

        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.max(1, Math.min(photoW / targetW, photoH / targetH));

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(imagePath, bmOptions);
        imageView.setImageBitmap(bitmap);
    }

    public void onClickDateField(View view) {
        final Calendar cldr = Calendar.getInstance();
        int day = cldr.get(Calendar.DAY_OF_MONTH);
        int month = cldr.get(Calendar.MONTH);
        int year = cldr.get(Calendar.YEAR);
        // date picker dialog
        new DatePickerDialog(this, (pickerView, pickerYear, monthOfYear, dayOfMonth) ->
                datePeremption.setText(DATE_TIME_FORMATTER.format(new Date(pickerYear - 1900, monthOfYear, dayOfMonth)))
                , year, month, day)
                .show();
    }

    public void onClickButtonSave(View view) {
        Intent intent = new Intent(this, Detailsproduct.class);
        intent.putExtra("bitmap", imagePath);
        startActivity(intent);
    }

    public void scheduleNotification(@org.jetbrains.annotations.NotNull Product product) {//delay is after how much time(in millis) from current time you want to schedule the notification
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .setContentTitle(getString(R.string.notification_name))
                .setContentText(getString(R.string.notification_description) + product.getProduct_name())
                .setAutoCancel(true)
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setLargeIcon(getNotificationIcon(product))
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));

        Intent intent = new Intent(this, ListPicture.class);
        PendingIntent activity = PendingIntent.getActivity(this, 56256, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        builder.setContentIntent(activity);

        Notification notification = builder.build();

        Intent notificationIntent = new Intent(this, MyNotificationPublisher.class);
        notificationIntent.putExtra(MyNotificationPublisher.NOTIFICATION_ID, MyNotificationPublisher.NOTIFICATION_ID);
        notificationIntent.putExtra(MyNotificationPublisher.NOTIFICATION, notification);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 56256, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, /*product.getDate().getTime()*/SystemClock.elapsedRealtime()+3000, pendingIntent);
    }

    private Bitmap getNotificationIcon(Product product) {
        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;

        BitmapFactory.decodeFile(product.getImage(), bmOptions);

        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.max(1, Math.min(photoW / 100, photoH / 100));

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        return BitmapFactory.decodeFile(imagePath, bmOptions);
    }
}