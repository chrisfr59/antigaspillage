package fr.afpa.antigaspillage;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.app.Notification ;
import android.app.NotificationChannel ;
import android.content.Context ;
import static fr.afpa.antigaspillage.MainActivity.NOTIFICATION_CHANNEL_ID;


/**
 * Created by Elvis on <DATE-DU-JOUR>.
 */
public class MyNotificationPublisher extends BroadcastReceiver {

    public static String NOTIFICATION_ID = "ANTI_GASPI-notification_id";
    public static String NOTIFICATION = "ANTI_GASPI-notification";

    @Override
    public void onReceive(final Context context, Intent intent) {

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        Notification notification = intent.getParcelableExtra(NOTIFICATION);
        int notificationId = intent.getIntExtra(NOTIFICATION_ID, 0);
        notificationManager.notify(notificationId, notification);
    }
}
